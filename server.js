var express=require('express');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');

var app=express();
const flash=require("connect-flash");

const MongoClient = require('mongodb').MongoClient;
var bodyParser=require('body-parser');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


var url = 'mongodb://localhost:27017';

app.use(express.static(__dirname+'/public'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(flash());


app.use(
    cookieSession({
      maxAge: 30 * 24 * 60 * 60 * 1000,
      keys: ['ytthhhj']
    })
);

// Passport init
app.use(passport.initialize());
app.use(passport.session());


passport.serializeUser(function(user, done) {
    console.log('Serialize user:'+JSON.stringify(user));
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    console.log("Deserialize called....",user);
    done(null, user);
});


app.post('/login',passport.authenticate('local', { 
                            successRedirect: '/success',
                           failureRedirect: '/failure',
                           failureFlash: true })
});


app.get('/success', (req, res) => {
	res.status(200).send('login succesful');
});

app.get('/failure', (req, res) => {
	res.status(401).send('Invalid');
});


app.listen(3000);
console.log('server running on port 3000');


var express=require('express');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
var app=express();
const flash=require("connect-flash");

const MongoClient = require('mongodb').MongoClient;
var bodyParser=require('body-parser');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var url = 'mongodb://localhost:27017';

app.use(cookieParser());
app.use(bodyParser.json());
app.use(flash());


app.use(
    cookieSession({
      maxAge: 30 * 24 * 60 * 60 * 1000,
      keys: ['ytthhhj']
    })
);

// Passport init
app.use(passport.initialize());
app.use(passport.session());



passport.use(new LocalStrategy(
     function(username, password, done) {
     	var url = 'mongodb://localhost:27017/litmus-users';
         MongoClient.connect(url, (err, db) => {
             if(err) {
                 throw err;
             } 
             db.collection('managers').find({ username: username}).toArray().then((user) => {
                 console.log(user);
                 if (err) { return done(err); }
                 if (user.length < 0) {
                 	console.log('user not find');
                     return done(null, false, { message: 'Incorrect username.' });
                 }
                 if (user[0].password !== password) {
                 	console.log('password not matched');
                     return done(null, false, { message: 'Incorrect password.' });
                 }
                 console.log('Success'+JSON.stringify(user[0]));
                return done(null, user[0]);
             }, (err) => {
                 throw err;
             });
             db.close();
         });
     })
 );



passport.serializeUser(function(user, done) {
    console.log('Serialize user:'+JSON.stringify(user));
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    console.log("Deserialize called....",user);
    done(null, user);
});


app.post('/login',passport.authenticate('local', { 
                            successRedirect: '/success',
                           failureRedirect: '/failure',
                           failureFlash: true }), function(req, res) {
   
                            res.redirect('/users/' + req.user.username);
});


app.get('/success', (req, res) => {
	res.status(200).send('login succesful');
});

app.get('/failure', (req, res) => {
	res.status(401).send('Invalid');
});

